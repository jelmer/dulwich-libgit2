#!/usr/bin/python
# Setup file for dulwich
# Copyright (C) 2008-2010 Jelmer Vernooij <jelmer@samba.org>

try:
    from setuptools import setup, Extension
except ImportError:
    from distutils.core import setup, Extension
from distutils.core import Distribution
import commands

dulwich_version_string = '0.7.0'

include_dirs = []
# Windows MSVC support
import sys
if sys.platform == 'win32':
    include_dirs.append('dulwich')


def pkgconfig(*packages, **kw):
    flag_map = {'-I': 'include_dirs', '-L': 'library_dirs', '-l': 'libraries'}
    (exitstatus, text) = commands.getstatusoutput("pkg-config --libs --cflags %s" % ' '.join(packages))
    if exitstatus != 0:
        raise Exception("Unable to find %s" % " ".join(packages))
    for token in text.split():
        kw.setdefault(flag_map.get(token[:2]), []).append(token[2:])
    assert isinstance(kw, dict)
    return kw


class DulwichDistribution(Distribution):

    def is_pure(self):
        if self.pure:
            return True

    def has_ext_modules(self):
        return not self.pure

    global_options = Distribution.global_options + [
        ('pure', None, 
            "use pure (slower) Python code instead of C extensions")]

    pure = False


setup(name='dulwich',
      description='Pure-Python Git Library',
      keywords='git',
      version=dulwich_version_string,
      url='http://samba.org/~jelmer/dulwich',
      download_url='http://samba.org/~jelmer/dulwich/dulwich-%s.tar.gz' % dulwich_version_string,
      license='GPLv2 or later',
      author='Jelmer Vernooij',
      author_email='jelmer@samba.org',
      long_description="""
      Simple Pure-Python implementation of the Git file formats and
      protocols. Dulwich is the place where Mr. and Mrs. Git live
      in one of the Monty Python sketches.
      """,
      packages=['dulwich', 'dulwich.tests'],
      scripts=['bin/dulwich', 'bin/dul-daemon', 'bin/dul-web'],
      ext_modules = [
          Extension('dulwich._index', ['dulwich/_index.c'],
                    **pkgconfig("libgit2", include_dirs=include_dirs)),
          Extension('dulwich._objects', ['dulwich/_objects.c'],
                    **pkgconfig("libgit2", include_dirs=include_dirs)),
          Extension('dulwich._pack', ['dulwich/_pack.c'],
                   **pkgconfig("libgit2", include_dirs=include_dirs)),
          ],
      distclass=DulwichDistribution,
      )

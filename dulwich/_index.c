/**
 * index.py -- File parser/writer for the git index file
 * Copyright (C) 2008-2009 Jelmer Vernooij <jelmer@samba.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License or (at your opinion) any later version of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <Python.h>
#include <git/index.h>

staticforward PyTypeObject PyIndexType;

typedef struct {
	PyObject_HEAD
	char *path;
	git_index *index;
} PyIndexObject;

static PyObject *py_index_read(PyIndexObject *self)
{
	git_index *index;
	PyIndexObject *ret;

	if (self->index != NULL) {
		git_index_free(self->index);
		return NULL;
	}

	index = git_index_alloc(self->path);
	if (index == NULL) {
		PyErr_Format(PyExc_RuntimeError, "Unable to open index file %s", self->path);
		return NULL;
	}

	ret->index = index;

	return (PyObject *)ret;
}

static PyMethodDef py_index_type_methods[] = {
	{ "read", (PyCFunction)py_index_read, METH_NOARGS, "Read current contents of index from disk." },
	{ NULL }
};

static int py_index_type_dealloc(PyIndexObject *self)
{
	git_index_free(self->index);
	PyObject_Del(self);
	return 0;
}

static PyObject *py_index_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
	char *kwnames[] = { "path", NULL };
	PyIndexObject *ret;
	char *path;
	git_index *index;
	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|s", &kwnames, &path))
		return NULL;

	index = git_index_read(path);
	if (index == NULL) {
		PyErr_Format(PyExc_RuntimeError, "Unable to open index file %s", path);
		return NULL;
	}

	ret = PyObject_New(PyIndexObject, type);
	if (ret == NULL) {
		return NULL;
	}
	ret->path = strdup(path);
	ret->index = index;

	return (PyObject *)ret;
}

static PyTypeObject PyIndexType = {
	PyObject_HEAD_INIT(NULL) 0,
	"_index.Index", /*	const char *tp_name;  For printing, in format "<module>.<name>" */
	sizeof(PyIndexObject), 
	0,/*	Py_ssize_t tp_basicsize, tp_itemsize;  For allocation */

	/* Methods to implement standard operations */

	(destructor)py_index_type_dealloc, /*	destructor tp_dealloc;	*/
	NULL, /*	printfunc tp_print;	*/
	NULL, /*	getattrfunc tp_getattr;	*/
	NULL, /*	setattrfunc tp_setattr;	*/
	NULL, /*	cmpfunc tp_compare;	*/
	NULL, /*	reprfunc tp_repr;	*/

	/* Method suites for standard classes */

	NULL, /*	PyNumberMethods *tp_as_number;	*/
	NULL, /*	PySequenceMethods *tp_as_sequence;	*/
	NULL, /*	PyMappingMethods *tp_as_mapping;	*/

	/* More standard operations (here for binary compatibility) */

	NULL, /*	hashfunc tp_hash;	*/
	NULL, /*	ternaryfunc tp_call;	*/
	NULL, /*	reprfunc tp_str;	*/
	NULL, /*	getattrofunc tp_getattro;	*/
	NULL, /*	setattrofunc tp_setattro;	*/

	/* Functions to access object as input/output buffer */
	NULL, /*	PyBufferProcs *tp_as_buffer;	*/

	/* Flags to define presence of optional/expanded features */
	0, /*	long tp_flags;	*/

	NULL, /*	const char *tp_doc;  Documentation string */

	/* Assigned meaning in release 2.0 */
	/* call function for all accessible objects */
	NULL, /*	traverseproc tp_traverse;	*/

	/* delete references to contained objects */
	NULL, /*	inquiry tp_clear;	*/

	/* Assigned meaning in release 2.1 */
	/* rich comparisons */
	NULL, /*	richcmpfunc tp_richcompare;	*/

	/* weak reference enabler */
	0, /*	Py_ssize_t tp_weaklistoffset;	*/

	/* Added in release 2.2 */
	/* Iterators */
	NULL, /*	getiterfunc tp_iter;	*/
	NULL, /*	iternextfunc tp_iternext;	*/

	/* Attribute descriptor and subclassing stuff */
	py_index_type_methods, /*	struct PyMethodDef *tp_methods;	*/

	NULL, /* struct PyMemberDef *tp_members; */
	NULL, /* struct PyGetSetDef *tp_getset; */
	NULL, /* struct _typeobject *tp_base; */
	NULL, /* PyObject *tp_dict; */
	NULL, /* descrgetfunc tp_descr_get; */
	NULL, /* descrsetfunc tp_descr_set; */
	0, /* Py_ssize_t tp_dictoffset; */
	NULL, /* initproc tp_init; */
	NULL, /* allocfunc tp_alloc; */
	py_index_new, /* tp_new */
};

void init_index(void)
{
	PyObject *m;

	if (PyType_Ready(&PyIndexType) < 0)
		return;

	m = Py_InitModule3("_index", NULL, NULL);
	if (m == NULL)
		return;

	PyModule_AddObject(m, "Index", (PyObject *)&PyIndexType);
	Py_INCREF(&PyIndexType);
}
